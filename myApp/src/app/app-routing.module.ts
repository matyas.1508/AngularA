import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DraftComponent } from './components/draft/draft.component';
import { GodsComponent } from './components/gods/gods.component';
import { MainComponent } from './components/main/main.component';
import { TeamsComponent } from './components/teams/teams.component';
import { UniversesComponent } from './components/universes/universes.component';
import { UniverseComponent } from './components/universe/universe.component';
import { ZenoComponent } from './components/zeno/zeno.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from "./components/login/login.component";
import { UsersComponent } from './components/users/users.component';
import { UserComponent } from './components/user/user.component';
import { PostsComponent } from './components/posts/posts.component';
import { PostComponent } from './components/post/post.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'draft', component: DraftComponent },
  { path: 'gods', component: GodsComponent },
  { path: 'main', component: MainComponent },
  { path: 'teams', component: TeamsComponent },
  { path: 'universes', component: UniversesComponent },
  { path: 'universe/:name', component: UniverseComponent },
  { path: 'users', component: UsersComponent },
  { path: 'user/:id', component: UserComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'post/:id', component: PostComponent },
  { path: 'zeno', component: ZenoComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
