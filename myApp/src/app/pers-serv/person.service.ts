import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class PersonService {

  person: any;
  canLog = true;
  auth = false;

  constructor() { }

  getPerson() {
    return Observable.of(this.person)
  }

  savePerson(person) {
    this.person = person;
  }

  checkPerson(lForm) {
    if (lForm.user == this.person.user && lForm.password == this.person.password) {
      this.canLog = true;
      return Observable.of(this.canLog)
    }
  }
}
