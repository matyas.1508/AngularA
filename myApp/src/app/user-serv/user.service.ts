import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { User } from '../user.interface';
import { Http } from '@angular/http';

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  getUserlist(): Observable<User[]> {
    return this.http
      .get('https://jsonplaceholder.typicode.com/users')
      .map(data => data.json());
  }

  getUser(id: number) {
    return this.http
    .get(`https://jsonplaceholder.typicode.com/users/${id}`)
    .map(data => data.json());
  }
}
