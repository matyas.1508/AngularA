import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../user-serv/user.service';
import { User } from '../../user.interface';
import { PostService } from '../../post-serv/post.service';
import { Post } from '../../post.interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user?: User;
  posts: Post[] = [];

  constructor(private route: ActivatedRoute, private userService: UserService, private postService: PostService) { }

  private getUser(id: number) {
    this.userService.getUser(id).subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const id = params['id'];
      this.getUser(id);
    });
    this.postService.getPosts().subscribe(posts => {
      this.posts = posts;
    });
  }

}
