import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UnivServService } from '../../univ-serv/univ-serv.service';
import { Universe } from '../../universe.interface';

@Component({
  selector: 'app-universe',
  templateUrl: './universe.component.html',
  styleUrls: ['./universe.component.scss']
})
export class UniverseComponent implements OnInit {

  universe?: Universe;

  constructor(private route: ActivatedRoute, private service: UnivServService) { }

  private getUniverse(name: string) {
    this.service.getUniverse(name).subscribe(universe => {
      this.universe = universe;
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const name = params['name'];
      this.getUniverse(name);
    });
  }

}
