import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../pers-serv/person.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  lForm: FormGroup;
  person: any;
  canLog: boolean;

  constructor(private formbuilder: FormBuilder, private personService: PersonService) {
    this.createForm();
  }

  private createForm() {
    this.lForm = this.formbuilder.group({
      'user': [null, Validators.required],
      'password': [null, Validators.required]
    })
  }

  ngOnInit() { }

  onSubmit() {
    this.personService.checkPerson(this.lForm.value).subscribe(data => {
      this.canLog = data;
    });
  }
  
  
}
