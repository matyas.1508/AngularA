import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user-serv/user.service';
import { User } from '../../user.interface';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUserlist().subscribe(users => {
      this.users = users;
    });
  }

}
