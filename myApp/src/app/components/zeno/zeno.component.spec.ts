import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZenoComponent } from './zeno.component';

describe('ZenoComponent', () => {
  let component: ZenoComponent;
  let fixture: ComponentFixture<ZenoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZenoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZenoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
