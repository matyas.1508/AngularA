import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbAccordionComponent } from './fb-accordion.component';

describe('FbAccordionComponent', () => {
  let component: FbAccordionComponent;
  let fixture: ComponentFixture<FbAccordionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbAccordionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
