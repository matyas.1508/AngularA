import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../../post-serv/post.service';
import { Post } from '../../post.interface';
import { CommentService } from '../../comm-serv/comment.service';
import { Comment } from '../../comment.interface';
import { UserService } from '../../user-serv/user.service';
import { User } from '../../user.interface';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  post?: Post;
  comments: Comment[] = [];
  users: User[] = [];

  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private postService: PostService, private commentService: CommentService, private userService: UserService) { }

  private getPost(id: number) {
    this.postService.getPost(id).subscribe(post => {
      this.post = post;
    });
  }

  private getComments(id: number) {
    this.commentService.getComments(id).subscribe(comments => {
      this.comments = comments;
    });
  }

  private getUserlist() {
    this.userService.getUserlist().subscribe(users => {
      this.users = users;
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const id = params['id'];
      this.getPost(id);
      this.getComments(id);
      this.getUserlist();
    });
  }

}
