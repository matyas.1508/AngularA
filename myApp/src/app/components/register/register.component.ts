import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from './register-password-matcher.class';
import { PersonService } from '../../pers-serv/person.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  rForm: FormGroup;
  titleAlert = 'This field is required';

  constructor(private formbuilder: FormBuilder, private personService: PersonService) {
    this.createForm();
  }

  private createForm() {
    this.rForm = this.formbuilder.group({
      'user': [null, [Validators.required, Validators.minLength(4)]],
      'password': [null, [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
      'rPassword': [null, [Validators.required]],
      'email': [null, [Validators.required, Validators.email]],
      'checkbox': [false, [Validators.requiredTrue]],
      'validate': ''
    }, {
      validator: PasswordValidation.MatchPassword
    });
  }

  onSubmit() {
    this.personService.savePerson(this.rForm.value);
  }

  ngOnInit() {
  }

}
