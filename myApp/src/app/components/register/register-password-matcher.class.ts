import { AbstractControl } from '@angular/forms';

export class PasswordValidation {
    static MatchPasswordValues(password: string, rPassword: string) {
        if (password === rPassword) {
            return null;
        }
        return {
            matchPassword: true
        };
    }

    static MatchPassword(controls: AbstractControl) {
        const password = controls.get('password').value;
        const rPassword = controls.get('rPassword').value;
        if (password === rPassword) {
            return null;
        } else {
            return {
                matchPassword: true
            };
        }
    }
}
