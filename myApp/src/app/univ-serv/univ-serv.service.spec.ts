import { TestBed, inject } from '@angular/core/testing';

import { UnivServService } from './univ-serv.service';

describe('UnivServService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnivServService]
    });
  });

  it('should be created', inject([UnivServService], (service: UnivServService) => {
    expect(service).toBeTruthy();
  }));
});
