import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';

@Injectable()
export class UnivServService {

  constructor(private http: Http) { }

  getAll() {
    return this.http
    .get('/json/universes.json')
    .map(data => data.json());
  }

  getUniverse(name: string) {
    return this.http
    .get(`/json/universe-${name}.json`)
    .map(data => data.json());
  }

}
