export interface Universe {
    'name': string;
    'members': {
        'deities': Object[],
        'fighters': Object[]
    };
}
