import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Comment } from '../comment.interface';
import { Http } from '@angular/http';

@Injectable()
export class CommentService {

  constructor(private http: Http) { }

  getComments(id: number): Observable<Comment[]> {
    return this.http
      .get(`https://jsonplaceholder.typicode.com/comments?postId=${id}`)
      .map(data => data.json());
  }

  getComment(id: number) {
    return this.http
    .get(`https://jsonplaceholder.typicode.com/comments/${id}`)
    .map(data => data.json());
  }
}
