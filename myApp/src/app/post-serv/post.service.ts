import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Post } from '../post.interface';
import { Http } from '@angular/http';

@Injectable()
export class PostService {

  constructor(private http: Http) { }

  getPosts(): Observable<Post[]> {
    return this.http
      .get('https://jsonplaceholder.typicode.com/posts')
      .map(data => data.json());
  }

  getPost(id: number) {
    return this.http
    .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .map(data => data.json());
  }
}
