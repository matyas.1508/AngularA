import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MainComponent } from './components/main/main.component';
import { UniversesComponent } from './components/universes/universes.component';
import { DraftComponent } from './components/draft/draft.component';
import { TeamsComponent } from './components/teams/teams.component';
import { GodsComponent } from './components/gods/gods.component';
import { ZenoComponent } from './components/zeno/zeno.component';
import { FbAccordionComponent } from './components/fb-accordion/fb-accordion.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { WorkingComponent } from './components/working/working.component';
import { UniverseComponent } from './components/universe/universe.component';
import { UserComponent } from './components/user/user.component';
import { UsersComponent } from './components/users/users.component';
import { PostsComponent } from './components/posts/posts.component';
import { PostComponent } from './components/post/post.component';

import { UnivServService } from './univ-serv/univ-serv.service';
import { UserService } from './user-serv/user.service';
import { PostService } from './post-serv/post.service';
import { CommentService } from './comm-serv/comment.service';
import { PersonService } from './pers-serv/person.service';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    MainComponent,
    UniversesComponent,
    DraftComponent,
    TeamsComponent,
    GodsComponent,
    ZenoComponent,
    FbAccordionComponent,
    WorkingComponent,
    RegisterComponent,
    LoginComponent,
    UniverseComponent,
    UserComponent,
    UsersComponent,
    UserComponent,
    PostsComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgbModule.forRoot(),
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    NgxPaginationModule
  ],
  providers: [
    UnivServService,
    UserService,
    PostService,
    CommentService,
    PersonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
